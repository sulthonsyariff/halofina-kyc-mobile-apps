import Api from '@/services/Api'
var format = require('date-fns/format')

// Reset Local Storage
export function resetLocalStorage () {
  let token = localStorage.getItem('halofina-token')
  let saving = localStorage.getItem('halofina-saving-profile')
  let features = JSON.parse(localStorage.getItem('halofina-features'))
  let fcmToken = localStorage.getItem('halofina-fcm-token')
  let attemptDate = localStorage.getItem('halofina-failed-attempt')
  let route = localStorage.getItem('halofina-route')
  let email = localStorage.getItem('halofina-email')
  let namespace = localStorage.getItem('halofina-namespace')
  let income = localStorage.getItem('halofina-income')
  let emasDialog = localStorage.getItem('halofina-emas-disclaimer')
  let kycStatus = localStorage.getItem('halofina-kyc-card')
  let lifeplanDialog = localStorage.getItem('halofina-dialog-lifeplan')
  let tourLifeplan = localStorage.getItem('halofina-tour-lifeplan')
  let rpaDialog = localStorage.getItem('halofina-dialog-rpa')
  let conversionData = localStorage.getItem('halofina-conversion-data')
  let googleData = localStorage.getItem('halofina-google-user')
  let mixpanelId = localStorage.getItem('mp_' + process.env.MIXPANELS_TOKEN + '_mixpanel')
  setTimeout(() => {
      if (features) {
          if (Array.isArray(features) === true) {
            localStorage.setItem('halofina-features', JSON.stringify(features))
          } else {
            let output = Object.entries(features).map(([feature, version]) => ({feature, version}))
            localStorage.setItem('halofina-features', JSON.stringify(output))
          }
      }
      if (!localStorage.getItem('halofina-refreshed')) {
          localStorage.clear()
          localStorage.setItem('halofina-refreshed', 'refreshed')
          if (token) {
              localStorage.setItem('halofina-token', token)
          }
          if (namespace) {
            localStorage.setItem('halofina-namespace', namespace)
          }
          if (fcmToken) {
              localStorage.setItem('halofina-fcm-token', fcmToken)
          }
          if (attemptDate) {
              localStorage.setItem('halofina-failed-attempt', attemptDate)
          }
          if (mixpanelId) {
            localStorage.setItem('mp_' + process.env.MIXPANELS_TOKEN + '_mixpanel', mixpanelId)
          }
          if (email) {
              localStorage.setItem('halofina-email', email)
          }
          if (saving) {
              localStorage.setItem('halofina-saving-profile', saving)
          }
          if (route) {
              localStorage.setItem('halofina-route', route)
          }
          if (emasDialog) {
              localStorage.setItem('halofina-emas-disclaimer', emasDialog)
          }
          if (kycStatus) {
              localStorage.setItem('halofina-kyc-card', kycStatus)
          }
          if (lifeplanDialog) {
              localStorage.setItem('halofina-dialog-lifeplan', lifeplanDialog)
          }
          if (tourLifeplan) {
              localStorage.setItem('halofina-tour-lifeplan', tourLifeplan)
          }
          if (rpaDialog) {
              localStorage.setItem('halofina-dialog-rpa', rpaDialog)
          }
          if (income) {
              localStorage.setItem('halofina-income', income)
          }
          if (conversionData) {
              localStorage.setItem('halofina-conversion-data', conversionData)
          }
          if (googleData) {
              localStorage.setItem('halofina-google-user', googleData)
          }
      }
  }, 300)
}

// return min height
export function minHeight () {
  return 'min-height: 100vh'
}

// Rename object keys
export function renameObjectKeys (obj, fromKey, toKey) {
  obj[toKey] = obj[fromKey];
  delete obj[fromKey];
}

// Filter product
export function filterProduct (condition, products, key) {
  var filteredProducts = []
  if (condition === true) {
    filteredProducts = products.filter(product => product.product_code === key)
  }
  return filteredProducts
}

export function replaceDot (value) {
  return value.replace(/\./g, '')
}

// Date formatter
export function dateFormat (date, dateFormats) {
  let idLocale = require('date-fns/locale/id')
  return format(date, dateFormats, {locale: idLocale})
}

export function checkFeatures (featureName) {
  // feature lists
  // "halofina:risk_profile"
  // "halofina:saving_profile"
  // "halofina:lifeplan_creator"
  // "halofina:profile_editor"
  // "halofina:kyc_page"
  // "halofina:finapedia"
  // "halofina:transaction_history"
  // "halofina:add_saving"
  // "halofina:password"
  // "halofina:pin"
  // "halofina:portfolio_model"
  // "halofina:email_otp"
  // "tanamduit:product"
  // "tanamduit:buy"
  // "tanamduit:sell"
  // "tanamduit:tracking"
  // "tanamduit:portfolio"
  // "tamasia:get"
  var features = JSON.parse(localStorage.getItem('halofina-features'))
  var environment = process.env.SENTRY_ENV
  var featureObj = {
    enabled: false,
    version: null,
    environment: environment
  }
  if (features) {
    features.forEach(element => {
      if (element.feature === featureName) {
        featureObj.enabled = true
        featureObj.version = Number(element.version)
      }
    })
  }
  return featureObj
}

export function getName () {
  Api('finacheck/kyc/personal', {})
    .then((res) => {
      localStorage.setItem('halofina-name', JSON.stringify(res.d.fullname))
    })
    .catch()
}

export function setBackRouteToAndroid (route) {
  if (typeof android !== 'undefined') android.getBackRouteFromWeb(route)
}

export function setPullToRefresh (condition) {
  if (typeof android !== 'undefined') android.setPullToRefresh(condition)
}

export function getOS () {
    var platform = window.navigator.platform
    var userAgent = window.navigator.userAgent
    var iosPlatforms = ['iPhone', 'iPad', 'iPod']
    var os = null
    if (iosPlatforms.indexOf(platform) !== -1) {
      os = 'ios'
    } else if (/Android/.test(userAgent)) {
      os = 'android'
    } else {
      os = 'others'
    }
    return os
}

export function capitalizeWord (word) {
    return word.charAt(0).toUpperCase() + word.slice(1)
}

export function numberingNumber (number, toFixed) {
  if (toFixed) {
    return Number(number).toLocaleString(['ban', 'id'], {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
  } else {
    return Number(number).toLocaleString(['ban', 'id'])
  }
}

export function numberingReksadana (number) {
  return Number(number).toLocaleString(['ban', 'id'], {
    minimumFractionDigits: 4,
    maximumFractionDigits: 4
  })
}

export function roundUp (num, precision) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}

export function setRouteToLocalStorage (route) {
  localStorage.setItem('halofina-route', route)
}

export function replaceDotWithComa (word) {
  return String(word).replace(/\./g, ',')
}

export const dateDatas = {
  tanggal: [{
    text: '1',
    value: '01'
  }, {
    text: '2',
    value: '02'
  }, {
    text: '3',
    value: '03'
  }, {
    text: '4',
    value: '04'
  }, {
    text: '5',
    value: '05'
  }, {
    text: '6',
    value: '06'
  }, {
    text: '7',
    value: '07'
  }, {
    text: '8',
    value: '08'
  }, {
    text: '9',
    value: '09'
  }, {
    text: '10',
    value: '10'
  }, {
    text: '11',
    value: '11'
  }, {
    text: '12',
    value: '12'
  }, {
    text: '13',
    value: '13'
  }, {
    text: '14',
    value: '14'
  }, {
    text: '15',
    value: '15'
  }, {
    text: '16',
    value: '16'
  }, {
    text: '17',
    value: '17'
  }, {
    text: '18',
    value: '18'
  }, {
    text: '19',
    value: '19'
  }, {
    text: '20',
    value: '20'
  }, {
    text: '21',
    value: '21'
  }, {
    text: '22',
    value: '22'
  }, {
    text: '23',
    value: '23'
  }, {
    text: '24',
    value: '24'
  }, {
    text: '25',
    value: '25'
  }, {
    text: '26',
    value: '26'
  }, {
    text: '27',
    value: '27'
  }, {
    text: '28',
    value: '28'
  }, {
    text: '29',
    value: '29'
  }, {
    text: '30',
    value: '30'
  }, {
    text: '31',
    value: '31'
  }],
  bulan: [{
    text: 'Jan',
    value: '01'
  }, {
    text: 'Feb',
    value: '02'
  }, {
    text: 'Mar',
    value: '03'
  }, {
    text: 'Apr',
    value: '04'
  }, {
    text: 'Mei',
    value: '05'
  }, {
    text: 'Jun',
    value: '06'
  }, {
    text: 'Jul',
    value: '07'
  }, {
    text: 'Ags',
    value: '08'
  }, {
    text: 'Sep',
    value: '09'
  }, {
    text: 'Okt',
    value: '10'
  }, {
    text: 'Nov',
    value: '11'
  }, {
    text: 'Des',
    value: '12'
  }],
  tahun: [{
    text: '2010',
    value: '2010'
  }, {
    text: '2009',
    value: '2009'
  }, {
    text: '2008',
    value: '2008'
  }, {
    text: '2007',
    value: '2007'
  }, {
    text: '2006',
    value: '2006'
  }, {
    text: '2005',
    value: '2005'
  }, {
    text: '2004',
    value: '2004'
  }, {
    text: '2003',
    value: '2003'
  }, {
    text: '2002',
    value: '2002'
  }, {
    text: '2001',
    value: '2001'
  }, {
    text: '2000',
    value: '2000'
  }, {
    text: '1999',
    value: '1999'
  }, {
    text: '1998',
    value: '1998'
  }, {
    text: '1997',
    value: '1997'
  }, {
    text: '1996',
    value: '1996'
  }, {
    text: '1995',
    value: '1995'
  }, {
    text: '1994',
    value: '1994'
  }, {
    text: '1993',
    value: '1993'
  }, {
    text: '1992',
    value: '1992'
  }, {
    text: '1991',
    value: '1991'
  }, {
    text: '1990',
    value: '1990'
  }, {
    text: '1989',
    value: '1989'
  }, {
    text: '1988',
    value: '1988'
  }, {
    text: '1987',
    value: '1987'
  }, {
    text: '1986',
    value: '1986'
  }, {
    text: '1985',
    value: '1985'
  }, {
    text: '1984',
    value: '1984'
  }, {
    text: '1983',
    value: '1983'
  }, {
    text: '1982',
    value: '1982'
  }, {
    text: '1981',
    value: '1981'
  }, {
    text: '1980',
    value: '1980'
  }, {
    text: '1979',
    value: '1979'
  }, {
    text: '1978',
    value: '1978'
  }, {
    text: '1977',
    value: '1977'
  }, {
    text: '1976',
    value: '1976'
  }, {
    text: '1975',
    value: '1975'
  }, {
    text: '1974',
    value: '1974'
  }, {
    text: '1973',
    value: '1973'
  }, {
    text: '1972',
    value: '1972'
  }, {
    text: '1971',
    value: '1971'
  }, {
    text: '1970',
    value: '1970'
  }, {
    text: '1969',
    value: '1969'
  }, {
    text: '1968',
    value: '1968'
  }, {
    text: '1967',
    value: '1967'
  }, {
    text: '1966',
    value: '1966'
  }, {
    text: '1965',
    value: '1965'
  }, {
    text: '1964',
    value: '1964'
  }, {
    text: '1963',
    value: '1963'
  }, {
    text: '1962',
    value: '1962'
  }, {
    text: '1961',
    value: '1961'
  }, {
    text: '1960',
    value: '1960'
  }, {
    text: '1959',
    value: '1959'
  }, {
    text: '1958',
    value: '1958'
  }, {
    text: '1957',
    value: '1957'
  }, {
    text: '1956',
    value: '1956'
  }, {
    text: '1955',
    value: '1955'
  }, {
    text: '1954',
    value: '1954'
  }, {
    text: '1953',
    value: '1953'
  }, {
    text: '1952',
    value: '1952'
  }, {
    text: '1951',
    value: '1951'
  }, {
    text: '1950',
    value: '1950'
  }, {
    text: '1949',
    value: '1949'
  }, {
    text: '1948',
    value: '1948'
  }, {
    text: '1947',
    value: '1947'
  }, {
    text: '1946',
    value: '1946'
  }, {
    text: '1945',
    value: '1945'
  }, {
    text: '1944',
    value: '1944'
  }, {
    text: '1943',
    value: '1943'
  }, {
    text: '1942',
    value: '1942'
  }, {
    text: '1941',
    value: '1941'
  }, {
    text: '1940',
    value: '1940'
  }, {
    text: '1939',
    value: '1939'
  }, {
    text: '1938',
    value: '1938'
  }, {
    text: '1937',
    value: '1937'
  }, {
    text: '1936',
    value: '1936'
  }, {
    text: '1935',
    value: '1935'
  }, {
    text: '1934',
    value: '1934'
  }, {
    text: '1933',
    value: '1933'
  }, {
    text: '1932',
    value: '1932'
  }, {
    text: '1931',
    value: '1931'
  }, {
    text: '1930',
    value: '1930'
  }, {
    text: '1929',
    value: '1929'
  }, {
    text: '1928',
    value: '1928'
  }, {
    text: '1927',
    value: '1927'
  }, {
    text: '1926',
    value: '1926'
  }, {
    text: '1925',
    value: '1925'
  }, {
    text: '1924',
    value: '1924'
  }, {
    text: '1923',
    value: '1923'
  }, {
    text: '1922',
    value: '1922'
  }, {
    text: '1921',
    value: '1921'
  }, {
    text: '1920',
    value: '1920'
  }, {
    text: '1919',
    value: '1919'
  }]
}
