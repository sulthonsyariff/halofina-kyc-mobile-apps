// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify, {
  VApp, // required
  VLayout,
  VContainer,
  VCardText,
  VFlex,
  VIcon,
  VResponsive,
  VBtn,
  VCard,
  VTextField
} from 'vuetify/lib'
import { Ripple } from 'vuetify/lib/directives'
import 'vuetify/src/stylus/app.styl'
import 'vue-smooth-picker/dist/css/style.css'
import SmoothPicker from 'vue-smooth-picker'
import money from 'v-money'
import VueMultianalytics from 'vue-multianalytics'
import '@/assets/style/index.scss'
require('vue-tour/dist/vue-tour.css')
import vueSignature from 'vue-signature'
import { track, setIdentify, setAlias, userProperties } from '@/services/Analytics.js'
import { setRouteToLocalStorage, minHeight, resetLocalStorage } from '@/services/Helpers.js'
import ReusableToolbar from '@/components/reusable/ReusableToolbar'
import ReusableDialogs from '@/components/reusable/ReusableDialogs'
import ReusableLoading from '@/components/reusable/ReusableLoading'
import ReusableBottomSheet from '@/components/reusable/ReusableBottomSheet'

resetLocalStorage()

// global function
Vue.prototype.$analytics = { track, setIdentify, setAlias, userProperties }
Vue.prototype.$helpers = { setRouteToLocalStorage, minHeight }

Vue.config.productionTip = false
Vue.use(money)
Vue.use(vueSignature)
Vue.use(SmoothPicker)
Vue.use(Vuetify, {
  components: {
    VApp,
    VLayout,
    VContainer,
    VCardText,
    VFlex,
    VIcon,
    VResponsive,
    VBtn,
    VCard,
    VTextField
  },
  directives: {
    Ripple
  },
  theme: {
    primary: '#42A5F5',
    secondary: '#8F9CF2',
    success: '#17CF82',
    error: '#F26969',
    warning: '#FFB245',
    info: '#42A5F5'
  }
})
Vue.use(VueMultianalytics, {
  modules: {
    mixpanel: {
      token: process.env.MIXPANEL_TOKEN,
      debug: process.env.MIXPANEL_DEBUG,
      config: {
        persistence: 'localStorage'
      }
    },
    routing: {
      vueRouter: router
    }
  }
})

Vue.component('reusable-toolbar', ReusableToolbar)
Vue.component('reusable-dialogs', ReusableDialogs)
Vue.component('reusable-loading', ReusableLoading)
Vue.component('reusable-bottom-sheet', ReusableBottomSheet)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
