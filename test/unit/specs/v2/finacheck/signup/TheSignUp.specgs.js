import Vue from 'vue'
import Vuetify from 'vuetify'
import TheSignUp from '@/components/v2/finacheck/signup/TheSignUp.vue'

Vue.use(Vuetify)

const Constructor = Vue.extend(TheSignUp)
const vm = new Constructor().$mount()

describe('TheSignUp.vue', () => {
//   it('should set form email correctly', () => {
//     // set data
//     vm.email = 'halofinatest3@gmail.com'
//     // test
//     Vue.nextTick().then(() => {
//       expect(vm.email).toBe('halofinatest3@gmail.com')
//     })
//   })

  it('should get token correctly', () => {
    let token = localStorage.getItem('halofina-token')

    vm.phone = '999888777666555'
    vm.email = 'yeay@yeay.com'
    vm.password.password = 'Pass1234'
    vm.name = 'Nama Yeay'

    vm.submit('halofinatest3@gmail.com')
      .then((res) => {
        expect(token).to.be()
      })
  })
})
