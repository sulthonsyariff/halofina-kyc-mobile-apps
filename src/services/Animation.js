import Lottie from "lottie-web";

export function animateAsset (element, path) {
  return Lottie.loadAnimation({
    container: element, // the dom element that will contain the animation
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: path // the path to the animation json
  })
}