import Vue from 'vue'
import Router from 'vue-router'
// import Api from '@/services/Api'

// import feature lists
import { checkFeatures } from '@/services/Helpers'

Vue.use(Router)

// KYC
const InputKycList = () => import('@/components/v2/finacheck/kyc/input/ListKyc')
const InputKycPersonal = () => import('@/components/v2/finacheck/kyc/input/Personal')
const InputKycAdditionalPersonal = () => import('@/components/v2/finacheck/kyc/input/AdditionalPersonal')
const InputKycJob = () => import('@/components/v2/finacheck/kyc/input/Job')
const InputKycAddress = () => import('@/components/v2/finacheck/kyc/input/Address')
const InputKycBank = () => import('@/components/v2/finacheck/kyc/input/Bank')
const InputKycEmail = () => import('@/components/v2/finacheck/kyc/input/Email')
const NewKycSK = () => import('@/components/v2/finacheck/kyc/input/SK')
const NewKycVerification = () => import('@/components/v2/finacheck/kyc/input/Verification')
const NewKycList = () => import('@/components/v2/finacheck/kyc/ListKyc')
const NewKycProcess = () => import('@/components/v2/finacheck/kyc/Process')
const NewProfileSettings = () => import('@/components/v2/finacheck/kyc/Settings')

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/v2/input/finacheck/kyc/list/:flag',
      component: InputKycList
    },
    {
      path: '/v2/input/finacheck/kyc/personal/:flag/:submit',
      component: InputKycPersonal
    },
    {
      path: '/v2/input/finacheck/kyc/additional_personal/:flag/:submit',
      component: InputKycAdditionalPersonal
    },
    {
      path: '/v2/input/finacheck/kyc/address/:flag/:submit',
      component: InputKycAddress
    },
    {
      path: '/v2/input/finacheck/kyc/bank/:flag/:submit',
      component: InputKycBank
    },
    {
      path: '/v2/input/finacheck/kyc/job/:flag/:submit',
      component: InputKycJob
    },
    {
      path: '/v2/input/finacheck/kyc/email',
      component: InputKycEmail
    },
    {
      path: '/v2/input/finacheck/kyc/sk',
      component: NewKycSK
    },
    {
      path: '/v2/input/finacheck/kyc/verification',
      component: NewKycVerification
    },
    // view in profile
    {
      path: '/v2/finacheck/kyc/list/new',
      component: NewKycList
    },
    {
      path: '/v2/finacheck/kyc/process',
      component: NewKycProcess
    },
    {
      path: '/profile/settings',
      component: NewProfileSettings
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  // let isAuthorized = checkToken()
  const canSetRpa = checkFeatures('halofina:risk_profile').enabled
  const hasRpa = localStorage.getItem('halofina-rpa')
  const hasSaving = localStorage.getItem('halofina-saving-profile')

  navigator.serviceWorker.register('service-worker.js').then(function (register) {
    register.update()
  })

  // KYC version
  // can access rpa
  if (canSetRpa === false && to.path === '/finacheck/rpa/input') {
    next('/dashboard')
  } else {
    next()
  }
  // has rpa
  if ((hasRpa === 'null' || hasSaving === 'null') && (to.path === '/v2/finacheck/kyc/list' || to.path === '/finacheck/kyc/list' || to.path === '/finavest/new/lifeplan/add')) {
    next('/new/finacheck/rpa/input/not')
  } else {
    next()
  }
})

export default router
